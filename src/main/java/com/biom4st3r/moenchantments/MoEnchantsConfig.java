package com.biom4st3r.moenchantments;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import biom4st3r.libs.biow0rks.BioLogger;
import net.fabricmc.loader.api.FabricLoader;

public class MoEnchantsConfig
{
    public static MoEnchantsConfig config;
    private static final BioLogger logger = new BioLogger("MoEnchantmentConfig");

    static {
        config = MoEnchantsConfig.init(config);
    }

    public boolean ApplyLootingToAlphaFire;

    public boolean UseStandardSoulboundMechanics;

    public int[] VeinMinerMaxBreakByLvl;

    public int[] TreeFellerMaxBreakByLvl;
    
    public int MaxDistanceFromPlayer;

    public boolean ProtectItemFromBreaking;

    public float AutoSmeltWoodModifier;

    public boolean TameProtectsOnlyYourAnimals;

    public float chanceForEnderCurseToTeleportv2;
    public float chanceForEnderCurseToPreventDamagev2;
    public float curse_of_the_end_teleport_range;

    public int perLevelChargeMultiplierForPotionRetention;

    public boolean AlphaFireCauseExtraDamage;
    
    public boolean ReduceElytraVelocityWithGrapnel;
    public double ReduceElytaVelocityWithGrapnelMultiplier;

    public boolean EnableSoulbound;

    public float slippery_drop_chance_on_swap;
    public float slippery_drop_chance_attack;
    public float slippery_multiplier_when_item_dropped_during_attack;
    public float slippery_dropChance_on_mine;
    public float slippery_drop_shield_when_hit;
    public float slippery_drop_on_item_use;

    public MoEnchantsConfig() {
        VeinMinerMaxBreakByLvl = new int[] {7, 14, 28};
        TreeFellerMaxBreakByLvl = new int[] {14, 28, 56};
        MaxDistanceFromPlayer = 8;
        ProtectItemFromBreaking = true;

        AutoSmeltWoodModifier = 0.15f;
        TameProtectsOnlyYourAnimals = true;
        chanceForEnderCurseToPreventDamagev2 = 0.2F;
        chanceForEnderCurseToTeleportv2 = 0.4F;
        perLevelChargeMultiplierForPotionRetention = 6;

        UseStandardSoulboundMechanics = false;

        ApplyLootingToAlphaFire = true;
        AlphaFireCauseExtraDamage = true;

        ReduceElytraVelocityWithGrapnel = true;
        ReduceElytaVelocityWithGrapnelMultiplier = 0.1D;

        EnableSoulbound = true;

        slippery_drop_chance_attack = 0.07F;
        slippery_drop_chance_on_swap = 0.01F;
        slippery_multiplier_when_item_dropped_during_attack = 0.1F;
        slippery_dropChance_on_mine = 0.04F;
        slippery_drop_shield_when_hit = 0.11F;
        slippery_drop_on_item_use = 0.01F;

        curse_of_the_end_teleport_range = 14;
    }

    public static MoEnchantsConfig init(MoEnchantsConfig config) {
        File file = new File(FabricLoader.getInstance().getConfigDir().toString(), "moenchantconfig.json");
        try {
            logger.log("loading config!");
            FileReader fr = new FileReader(file);
            config = new Gson().fromJson(fr, MoEnchantsConfig.class);
            FileWriter fw = new FileWriter(file);
            fw.write(new GsonBuilder().setPrettyPrinting().create().toJson(config));
            fw.close();
        } catch (IOException e) {
            logger.log("failed loading! Creating initial config!");
            config = new MoEnchantsConfig();
            try {
                FileWriter fw = new FileWriter(file);
                fw.write(new GsonBuilder().setPrettyPrinting().create().toJson(config));
                fw.close();
            } catch (IOException e1) {
                logger.log("failed config!");
                e1.printStackTrace();
            }
        }
        return config;
    }




    






}