package com.biom4st3r.moenchantments.interfaces;

import java.util.Optional;

import com.biom4st3r.moenchantments.logic.EndStepProgressCounter;

public interface EndStepper {
    void moenchantment$setStepProgress(Optional<EndStepProgressCounter> optional);
    Optional<EndStepProgressCounter> moenchantment$getStepProgress();
}
