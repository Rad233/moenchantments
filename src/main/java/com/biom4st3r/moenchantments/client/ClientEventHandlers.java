package com.biom4st3r.moenchantments.client;

import com.biom4st3r.moenchantments.logic.RetainedPotion;

import biom4st3r.libs.moenchant_lib.Romanifier;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.item.v1.ItemTooltipCallback;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableTextContent;
import net.minecraft.util.Formatting;

@Environment(EnvType.CLIENT)
public class ClientEventHandlers {
    public static void init() {
        ItemTooltipCallback.EVENT.register((stack, context, tips) -> {
            RetainedPotion.borrowRetainedPotion(stack, rt -> {
                if (rt.getCharges() <= 0) return;
                for (int i = 0; i < tips.size(); i++) {
                    Text text = tips.get(i);
                    if (text instanceof MutableText tt) {
                        if (tt.getContent() instanceof TranslatableTextContent ttc && ttc.getKey().equals("enchantment.moenchantments.potionretension")) {
                            String name = Text.translatable(rt.getEffect().getTranslationKey()).getString();
                            Text newText = Text.of(
                                String.format(
                                    "%s- %s %s%s %s/%s",
                                    Formatting.GRAY,
                                    name,
                                    Romanifier.toRoman(rt.getAmp()+1),
                                    Formatting.RESET,
                                    rt.getCharges(),
                                    rt.getMaxCharges()
                                ));
                            tips.add(i+1, newText);
                        }
                    }
                }
            });
        });
    }
}
