package com.biom4st3r.moenchantments;

import net.minecraft.item.Item;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

public final class NewFeatures {
    public static Item INFUSED_IRON_SHAVINGS;
    public static Item INFUSED_GOLD_SHAVINGS;
    public static Item INFUSED_NETHERITE_SHAVINGS;
    public static boolean new_features = false;

    public static void init() {
        new_features = true;
        INFUSED_IRON_SHAVINGS = new Item(new Item.Settings());
        INFUSED_GOLD_SHAVINGS = new Item(new Item.Settings());
        INFUSED_NETHERITE_SHAVINGS = new Item(new Item.Settings());
        Registry.register(Registries.ITEM, new Identifier(ModInit.MODID, "infused_iron_shavings"), INFUSED_IRON_SHAVINGS);
        Registry.register(Registries.ITEM, new Identifier(ModInit.MODID, "infused_gold_shavings"), INFUSED_GOLD_SHAVINGS);
        Registry.register(Registries.ITEM, new Identifier(ModInit.MODID, "infused_netherite_shavings"), INFUSED_NETHERITE_SHAVINGS);
    }
}
