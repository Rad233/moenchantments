package com.biom4st3r.moenchantments.loot;

import java.util.List;

import com.google.common.collect.Lists;

public class TableBuilder<T> {
    private List<Entry<T>> entries = Lists.newArrayList();

    public TableBuilder<T> add(Entry<T> entry) {
        this.entries.add(entry);
        return this;
    }

    @SuppressWarnings({"unchecked"})
    public Table<T> build() {
        System.out.println(entries.size());
        return new Table<T>(this.entries.toArray(Entry[]::new));
    }
}
