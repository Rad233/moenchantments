package com.biom4st3r.moenchantments.loot;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;
import java.util.stream.Stream;

public record Table<T>(Entry<T>[] entries) {

    static <T> Entry<T>[] sort(Entry<T>[] entries) {
        Arrays.sort(entries, (a,b) -> a.weight() < b.weight() ? 1 : a.weight() == b.weight() ? 0 : -1);
        return entries;
    }

    @SuppressWarnings({"unchecked"})
    public T roll(Random random, TableCtx ctx) {
        final long[] sum = {0};
        // stream and iter is created here in order to collect the sum of weight before rolling
        Entry<T>[] filtered = Stream
            .of(entries)
            .filter(entry -> entry.test(ctx))
            .peek(e -> sum[0]+=e.weight())
            .toArray(Entry[]::new);

        
        final long roll = random.nextLong(sum[0]);
        return roll(roll, Stream.of(filtered).iterator());
    }

    private static <T> T roll(long roll, Iterable<Entry<T>> entries) {
        long accum = 0;
        for (Entry<T> e : entries) {
            if (roll < accum + e.weight()) {
                return e.t();
            }
            accum += e.weight();
        }
        return null;
    }
    private static <T> T roll(long roll, Iterator<Entry<T>> entries) {
        return roll(roll, () -> entries);
    }

    public T rollNoTests(Random random) {
        long[] total = new long[1];
        Stream<Entry<T>> filtered = Stream.of(this.entries);

        return roll(random.nextLong(total[0]), filtered.iterator());
    }
}
