package com.biom4st3r.moenchantments.loot;

import java.util.function.Predicate;

public record Entry<T>(T t, int weight, Predicate<TableCtx> requirement) implements Predicate<TableCtx> {

    private static Predicate<TableCtx> merge(Predicate<TableCtx>[] tests) {
        for (int i = 1 ; i < tests.length; i++) {
            tests[0] = tests[0].and(tests[i]);
        }
        return tests[0];
    }
    public Entry(T t, int weight, Predicate<TableCtx>[] tests) {
        this(t, weight, merge(tests));
    }
    public static <T> Entry<T> of(T t, int weight, Predicate<TableCtx> tests) {
        return new Entry<T>(t, weight, tests);
    }
    @Override
    public boolean test(TableCtx arg0) {
        return this.requirement().test(arg0);
    }
}
