package com.biom4st3r.moenchantments.loot;

import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;

public final class TableCtx {
    private final Object2ObjectMap<CtxKey<?>,Object> map = new Object2ObjectOpenHashMap<>();

    @SuppressWarnings({"unchecked"})
    public <T> T get(CtxKey<T> key) {
        return (T) map.get(key);
    }
    public <T> TableCtx add(CtxKey<T> key, T t) {
        map.put(key, t);
        return this;
    }
    public boolean contains(CtxKey<?> key) {
        return map.containsKey(key);
    }
}
