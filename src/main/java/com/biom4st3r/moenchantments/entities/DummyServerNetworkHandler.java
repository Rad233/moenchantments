package com.biom4st3r.moenchantments.entities;

import net.minecraft.network.ClientConnection;
import net.minecraft.network.NetworkSide;
import net.minecraft.network.NetworkState;
import net.minecraft.network.Packet;
import net.minecraft.network.PacketCallbacks;
import net.minecraft.network.listener.PacketListener;
import net.minecraft.network.message.MessageType.Parameters;
import net.minecraft.network.message.SignedMessage;
import net.minecraft.network.packet.c2s.play.AdvancementTabC2SPacket;
import net.minecraft.network.packet.c2s.play.BoatPaddleStateC2SPacket;
import net.minecraft.network.packet.c2s.play.BookUpdateC2SPacket;
import net.minecraft.network.packet.c2s.play.ButtonClickC2SPacket;
import net.minecraft.network.packet.c2s.play.ChatMessageC2SPacket;
import net.minecraft.network.packet.c2s.play.ClickSlotC2SPacket;
import net.minecraft.network.packet.c2s.play.ClientCommandC2SPacket;
import net.minecraft.network.packet.c2s.play.ClientSettingsC2SPacket;
import net.minecraft.network.packet.c2s.play.ClientStatusC2SPacket;
import net.minecraft.network.packet.c2s.play.CloseHandledScreenC2SPacket;
import net.minecraft.network.packet.c2s.play.CommandExecutionC2SPacket;
import net.minecraft.network.packet.c2s.play.CraftRequestC2SPacket;
import net.minecraft.network.packet.c2s.play.CreativeInventoryActionC2SPacket;
import net.minecraft.network.packet.c2s.play.CustomPayloadC2SPacket;
import net.minecraft.network.packet.c2s.play.HandSwingC2SPacket;
import net.minecraft.network.packet.c2s.play.JigsawGeneratingC2SPacket;
import net.minecraft.network.packet.c2s.play.KeepAliveC2SPacket;
import net.minecraft.network.packet.c2s.play.MessageAcknowledgmentC2SPacket;
import net.minecraft.network.packet.c2s.play.PickFromInventoryC2SPacket;
import net.minecraft.network.packet.c2s.play.PlayPongC2SPacket;
import net.minecraft.network.packet.c2s.play.PlayerActionC2SPacket;
import net.minecraft.network.packet.c2s.play.PlayerInputC2SPacket;
import net.minecraft.network.packet.c2s.play.PlayerInteractBlockC2SPacket;
import net.minecraft.network.packet.c2s.play.PlayerInteractEntityC2SPacket;
import net.minecraft.network.packet.c2s.play.PlayerInteractItemC2SPacket;
import net.minecraft.network.packet.c2s.play.PlayerMoveC2SPacket;
import net.minecraft.network.packet.c2s.play.PlayerSessionC2SPacket;
import net.minecraft.network.packet.c2s.play.QueryBlockNbtC2SPacket;
import net.minecraft.network.packet.c2s.play.QueryEntityNbtC2SPacket;
import net.minecraft.network.packet.c2s.play.RecipeBookDataC2SPacket;
import net.minecraft.network.packet.c2s.play.RecipeCategoryOptionsC2SPacket;
import net.minecraft.network.packet.c2s.play.RenameItemC2SPacket;
import net.minecraft.network.packet.c2s.play.RequestCommandCompletionsC2SPacket;
import net.minecraft.network.packet.c2s.play.ResourcePackStatusC2SPacket;
import net.minecraft.network.packet.c2s.play.SelectMerchantTradeC2SPacket;
import net.minecraft.network.packet.c2s.play.SpectatorTeleportC2SPacket;
import net.minecraft.network.packet.c2s.play.TeleportConfirmC2SPacket;
import net.minecraft.network.packet.c2s.play.UpdateBeaconC2SPacket;
import net.minecraft.network.packet.c2s.play.UpdateCommandBlockC2SPacket;
import net.minecraft.network.packet.c2s.play.UpdateCommandBlockMinecartC2SPacket;
import net.minecraft.network.packet.c2s.play.UpdateDifficultyC2SPacket;
import net.minecraft.network.packet.c2s.play.UpdateDifficultyLockC2SPacket;
import net.minecraft.network.packet.c2s.play.UpdateJigsawC2SPacket;
import net.minecraft.network.packet.c2s.play.UpdatePlayerAbilitiesC2SPacket;
import net.minecraft.network.packet.c2s.play.UpdateSelectedSlotC2SPacket;
import net.minecraft.network.packet.c2s.play.UpdateSignC2SPacket;
import net.minecraft.network.packet.c2s.play.UpdateStructureBlockC2SPacket;
import net.minecraft.network.packet.c2s.play.VehicleMoveC2SPacket;
import net.minecraft.network.packet.s2c.play.PlayerPositionLookS2CPacket.Flag;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;

import io.netty.channel.ChannelHandlerContext;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;

import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.net.SocketAddress;
import java.util.Set;

import javax.crypto.Cipher;

public class DummyServerNetworkHandler extends ServerPlayNetworkHandler {

    // private static class DummyChannelConn extends 

    private static class DummyClientConn extends ClientConnection {

        public DummyClientConn(NetworkSide side) {
            super(side);
        }

        @Override
        public void channelActive(ChannelHandlerContext context) throws Exception {
        
        }

        @Override
        public void channelInactive(ChannelHandlerContext context) {
        
        }

        @Override
        protected void channelRead0(ChannelHandlerContext channelHandlerContext, Packet<?> packet) {
        
        }

        @Override
        public void disableAutoRead() {
        
        }

        @Override
        public void disconnect(Text disconnectReason) {
        
        }

        @Override
        public void exceptionCaught(ChannelHandlerContext context, Throwable ex) {
        
        }

        @Override
        public SocketAddress getAddress() {
            return super.getAddress();
        }

        @Override
        public float getAveragePacketsReceived() {
            return super.getAveragePacketsReceived();
        }

        @Override
        public float getAveragePacketsSent() {
            return super.getAveragePacketsSent();
        }

        @Override
        public Text getDisconnectReason() {
            return super.getDisconnectReason();
        }

        @Override
        public NetworkSide getOppositeSide() {
            return super.getOppositeSide();
        }

        @Override
        public PacketListener getPacketListener() {
            return super.getPacketListener();
        }

        @Override
        public NetworkSide getSide() {
            return super.getSide();
        }

        @Override
        public void handleDisconnection() {
        
        }

        @Override
        public boolean hasChannel() {
            return super.hasChannel();
        }

        @Override
        public boolean isEncrypted() {
            return super.isEncrypted();
        }

        @Override
        public boolean isLocal() {
            return super.isLocal();
        }

        @Override
        public boolean isOpen() {
            return super.isOpen();
        }

        @Override
        public void send(Packet<?> packet) {
        
        }

        // @Override
        // public void send(Packet<?> packet, GenericFutureListener<? extends Future<? super Void>> callback) {
        
        // }

        @Override
        public void setCompressionThreshold(int compressionThreshold, boolean rejectsBadPackets) {
        
        }

        @Override
        public void setPacketListener(PacketListener listener) {
        
        }

        @Override
        public void setState(NetworkState state) {
        
        }

        @Override
        public void setupEncryption(Cipher decryptionCipher, Cipher encryptionCipher) {
        
        }

        @Override
        public void tick() {
        
        }

        @Override
        protected void updateStats() {
        
        }

        @Override
        public boolean acceptInboundMessage(Object msg) throws Exception {
            return super.acceptInboundMessage(msg);
        }

        @Override
        public void channelRead(ChannelHandlerContext arg0, Object arg1) throws Exception {
        
        }

        @Override
        public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        
        }

        @Override
        public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
        
        }

        @Override
        public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        
        }

        @Override
        public void channelWritabilityChanged(ChannelHandlerContext ctx) throws Exception {
        
        }

        @Override
        public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        
        }

        @Override
        protected void ensureNotSharable() {
        
        }

        @Override
        public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        
        }

        @Override
        public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        
        }

        @Override
        public boolean isSharable() {
            return super.isSharable();
        }
        
    }

    public DummyServerNetworkHandler(MinecraftServer server, ServerPlayerEntity player) {
        super(server, new DummyClientConn(NetworkSide.CLIENTBOUND), player);
    }

    protected void adventure$initTracking(final MinecraftServer server, final ClientConnection conn, final ServerPlayerEntity player, final CallbackInfo ci) {
        
    }

    @Override
    public void disconnect(Text reason) {
    
    }

    @Override
    public ClientConnection getConnection() {
        return super.getConnection();
    }

    @Override
    public ServerPlayerEntity getPlayer() {
        return super.getPlayer();
    }

    @Override
    public void onAdvancementTab(AdvancementTabC2SPacket packet) {
    
    }

    @Override
    public void onBoatPaddleState(BoatPaddleStateC2SPacket packet) {
    
    }

    @Override
    public void onBookUpdate(BookUpdateC2SPacket packet) {
    
    }

    @Override
    public void onButtonClick(ButtonClickC2SPacket packet) {
    
    }

    @Override
    public void onChatMessage(ChatMessageC2SPacket packet) {
    
    }

    @Override
    public void onClickSlot(ClickSlotC2SPacket packet) {
    
    }

    @Override
    public void onClientCommand(ClientCommandC2SPacket packet) {
    
    }

    @Override
    public void onClientSettings(ClientSettingsC2SPacket packet) {
    
    }

    @Override
    public void onClientStatus(ClientStatusC2SPacket packet) {
    
    }

    @Override
    public void onCloseHandledScreen(CloseHandledScreenC2SPacket packet) {
    
    }

    @Override
    public void onCraftRequest(CraftRequestC2SPacket packet) {
    
    }

    @Override
    public void onCreativeInventoryAction(CreativeInventoryActionC2SPacket packet) {
    
    }

    @Override
    public void onCustomPayload(CustomPayloadC2SPacket packet) {
    
    }

    @Override
    public void onDisconnected(Text reason) {
    
    }

    @Override
    public void onHandSwing(HandSwingC2SPacket packet) {
    
    }

    @Override
    public void onJigsawGenerating(JigsawGeneratingC2SPacket packet) {
    
    }

    @Override
    public void onKeepAlive(KeepAliveC2SPacket packet) {
    
    }

    @Override
    public void onPickFromInventory(PickFromInventoryC2SPacket packet) {
    
    }

    @Override
    public void onPlayerAction(PlayerActionC2SPacket packet) {
    
    }

    @Override
    public void onPlayerInput(PlayerInputC2SPacket packet) {
    
    }

    @Override
    public void onPlayerInteractBlock(PlayerInteractBlockC2SPacket packet) {
    
    }

    @Override
    public void onPlayerInteractEntity(PlayerInteractEntityC2SPacket packet) {
    
    }

    @Override
    public void onPlayerInteractItem(PlayerInteractItemC2SPacket packet) {
    
    }

    @Override
    public void onPlayerMove(PlayerMoveC2SPacket packet) {
    
    }

    @Override
    public void onPong(PlayPongC2SPacket packet) {
    
    }

    @Override
    public void onQueryBlockNbt(QueryBlockNbtC2SPacket packet) {
    
    }

    @Override
    public void onQueryEntityNbt(QueryEntityNbtC2SPacket packet) {
    
    }

    @Override
    public void onRecipeBookData(RecipeBookDataC2SPacket packet) {
    
    }

    @Override
    public void onRecipeCategoryOptions(RecipeCategoryOptionsC2SPacket packet) {
    
    }

    @Override
    public void onRenameItem(RenameItemC2SPacket packet) {
    
    }

    @Override
    public void onRequestCommandCompletions(RequestCommandCompletionsC2SPacket packet) {
    
    }

    @Override
    public void onResourcePackStatus(ResourcePackStatusC2SPacket packet) {
    
    }

    @Override
    public void onSelectMerchantTrade(SelectMerchantTradeC2SPacket packet) {
    
    }

    @Override
    public void onSpectatorTeleport(SpectatorTeleportC2SPacket packet) {
    
    }

    @Override
    public void onTeleportConfirm(TeleportConfirmC2SPacket packet) {
    
    }

    @Override
    public void onUpdateBeacon(UpdateBeaconC2SPacket packet) {
    
    }

    @Override
    public void onUpdateCommandBlock(UpdateCommandBlockC2SPacket packet) {
    
    }

    @Override
    public void onUpdateCommandBlockMinecart(UpdateCommandBlockMinecartC2SPacket packet) {
    
    }

    @Override
    public void onUpdateDifficulty(UpdateDifficultyC2SPacket packet) {
    
    }

    @Override
    public void onUpdateDifficultyLock(UpdateDifficultyLockC2SPacket packet) {
    
    }

    @Override
    public void onUpdateJigsaw(UpdateJigsawC2SPacket packet) {
    
    }

    @Override
    public void onUpdatePlayerAbilities(UpdatePlayerAbilitiesC2SPacket packet) {
    
    }

    @Override
    public void onUpdateSelectedSlot(UpdateSelectedSlotC2SPacket packet) {
    
    }

    @Override
    public void onUpdateSign(UpdateSignC2SPacket packet) {
    
    }

    @Override
    public void onUpdateStructureBlock(UpdateStructureBlockC2SPacket packet) {
    
    }

    @Override
    public void onVehicleMove(VehicleMoveC2SPacket packet) {
    
    }

    @Override
    public void requestTeleport(double x, double y, double z, float yaw, float pitch) {
    
    }

    @Override
    public void requestTeleport(double x, double y, double z, float yaw, float pitch, Set<Flag> flags) {
    
    }

    @Override
    public void requestTeleport(double x, double y, double z, float yaw, float pitch, Set<Flag> flags,
            boolean shouldDismount) {
    
    }

    @Override
    public void requestTeleportAndDismount(double x, double y, double z, float yaw, float pitch) {
    
    }

    @Override
    public void sendPacket(Packet<?> packet) {
    
    }

    // @Override
    // public void sendPacket(Packet<?> packet, GenericFutureListener<? extends Future<? super Void>> listener) {
    
    // }
    @Override
    public void sendPacket(Packet<?> packet, PacketCallbacks callbacks) {

    }

    @Override
    public void syncWithPlayerPosition() {
    
    }

    @Override
    public void tick() {
    
    }

    @Override
    public boolean shouldCrashOnException() {
        return super.shouldCrashOnException();
    }

    @Override
    public void addPendingAcknowledgment(SignedMessage message) {
    }

    @Override
    public void onCommandExecution(CommandExecutionC2SPacket packet) {
    }

    @Override
    public void onMessageAcknowledgment(MessageAcknowledgmentC2SPacket packet) {
    }

    @Override
    public void onPlayerSession(PlayerSessionC2SPacket packet) {
    }

    @Override
    public void sendChatMessage(SignedMessage message, Parameters params) {
    }

    @Override
    public void sendProfilelessChatMessage(Text message, Parameters params) {
    }

    @Override
    public void updateSequence(int sequence) {
    }
    
}
