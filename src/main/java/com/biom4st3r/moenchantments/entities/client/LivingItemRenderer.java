package com.biom4st3r.moenchantments.entities.client;

import org.joml.Vector3f;

import com.biom4st3r.moenchantments.entities.Emote;
import com.biom4st3r.moenchantments.entities.LivingItemEntity;

import biom4st3r.libs.biow0rks.rendering.RenderHelper;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.Camera;
import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.render.entity.EntityRenderer;
import net.minecraft.client.render.entity.EntityRendererFactory.Context;
import net.minecraft.client.render.item.ItemRenderer;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.texture.SpriteAtlasTexture;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RotationAxis;

public class LivingItemRenderer extends EntityRenderer<LivingItemEntity> {

    public LivingItemRenderer(Context ctx) {
        super(ctx);
        itemRenderer = ctx.getItemRenderer();
    }

    private final ItemRenderer itemRenderer;

    public static void vertex(VertexConsumer vc, MatrixStack stack,float x,float y,float z,float u, float v,float r,float g,float b,float a,int overlayuv,int lightuv,float nx,float ny,float nz)
    {
        vc.vertex(stack.peek().getPositionMatrix(),x,y,z)
            .color(r,g,b,a)
            .texture(u, v)
            .overlay(OverlayTexture.DEFAULT_UV)
            .light(lightuv)
            .normal(stack.peek().getNormalMatrix(),nx,ny,nz)
            .next();
    }

    @SuppressWarnings({"resource"})
    private void renderEmote(LivingItemEntity entity, MatrixStack stack, VertexConsumerProvider vertexConsumers, int light) {
        if (entity.getEmote() == Emote.NONE) return;
        stack.push();
        float size = 0.4f;
        VertexConsumer vc = vertexConsumers.getBuffer(RenderLayer.getItemEntityTranslucentCull(entity.getEmote().identifier));
        stack.scale(0.5f, 0.5f, 0.5f);
        float[] colors = RenderHelper.unpackRGBA_F(entity.getEmote().color, new float[4]);
        stack.multiply(this.dispatcher.getRotation());
        stack.multiply(RotationAxis.POSITIVE_Y.rotationDegrees(180.0F));
        stack.translate(-size/2f, 0.80, 0);
        vertex(vc, stack, size, size, 0, 0,0, colors[0], colors[1], colors[2], colors[3], OverlayTexture.DEFAULT_UV, light, 1,1,1);// bottomleft
        vertex(vc, stack, 0, size, 0, 1,0, colors[0], colors[1], colors[2], colors[3], OverlayTexture.DEFAULT_UV, light, 1,1,1);//top left
        vertex(vc, stack, 0, 0, 0, 1,1, colors[0], colors[1], colors[2], colors[3], OverlayTexture.DEFAULT_UV, light, 1,1,1);// top right
        vertex(vc, stack, size, 0, 0, 0,1, colors[0], colors[1], colors[2], colors[3], OverlayTexture.DEFAULT_UV, light, 1,1,1);//bottom right
        stack.pop();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private void renderChest(BlockEntity blockEntity, MatrixStack stack, float tickDelta, VertexConsumerProvider vcp, int light) {
        stack.push();
        float scale = 0.15F;
        stack.scale(scale, scale, scale);


        stack.multiply(RotationAxis.POSITIVE_Y.rotationDegrees(90));
        stack.multiply(RotationAxis.POSITIVE_X.rotationDegrees(45));

        stack.translate(-0.5F, 0, -0.35);
        BlockEntityRenderer renderer = MinecraftClient.getInstance().getBlockEntityRenderDispatcher().get(blockEntity);

        if (renderer == null) {
            MinecraftClient.getInstance().getBlockRenderManager().renderBlockAsEntity(blockEntity.getCachedState(), stack, vcp, light, OverlayTexture.DEFAULT_UV);
        } else {
            renderer.render(blockEntity, tickDelta, stack, vcp, light, OverlayTexture.DEFAULT_UV);
        }
        stack.pop();
    }

    @Override
    public void render(LivingItemEntity entity, float yaw, float tickDelta, MatrixStack stack,
            VertexConsumerProvider vertexConsumers, int light) {
        
        stack.push();
        ItemStack itemStack = entity.getStack();
        BakedModel model = this.itemRenderer.getModel(itemStack, entity.world, null, entity.getId());
        float height = MathHelper.sin(((float)(entity.age) + tickDelta) / 10.0f) * 0.05f + 0.3f;
        Vector3f offset = model.getTransformation().ground.translation;
        Vector3f scale = model.getTransformation().ground.scale;
        stack.translate(offset.x, offset.y + height, offset.z); //  
        stack.scale(scale.x * 4, scale.y * 4, scale.z * 4);

        renderEmote(entity, stack, vertexConsumers, light);
        
        stack.multiply(RotationAxis.POSITIVE_Y.rotationDegrees(90 - entity.getYaw(tickDelta)));
        stack.multiply(RotationAxis.POSITIVE_Z.rotationDegrees(entity.getPitch(tickDelta) + 60));

        if (entity.hasBackpack()) {
            renderChest(entity.getBackpack(), stack, tickDelta, vertexConsumers, light);
        }

        this.itemRenderer.renderItem(itemStack, ModelTransformation.Mode.GROUND, false, stack, vertexConsumers, light, OverlayTexture.DEFAULT_UV, model);
        stack.pop();

        super.render(entity, yaw, tickDelta, stack, vertexConsumers, light);
    }

    @Override
    @SuppressWarnings({"deprecation"})
    public Identifier getTexture(LivingItemEntity var1) {
        return SpriteAtlasTexture.BLOCK_ATLAS_TEXTURE;
    }
    
}
