package com.biom4st3r.moenchantments.logic;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;

import com.biom4st3r.moenchantments.MoEnchantsConfig;
import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.util.SingletonInventory;
import com.biom4st3r.moenchantments.util.TagHelper;
import com.google.common.collect.Sets;

import net.minecraft.block.Block;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.entry.AlternativeEntry;
import net.minecraft.loot.entry.LeafEntry;
import net.minecraft.loot.entry.LootPoolEntry;
import net.minecraft.loot.entry.LootPoolEntryTypes;
import net.minecraft.loot.function.ApplyBonusLootFunction;
import net.minecraft.loot.function.ApplyBonusLootFunction.OreDrops;
import net.minecraft.loot.function.LootFunction;
import net.minecraft.recipe.RecipeManager;
import net.minecraft.recipe.RecipeType;
import net.minecraft.recipe.SmeltingRecipe;
import net.minecraft.registry.Registries;
import net.minecraft.registry.entry.RegistryEntryList.Named;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * AlphafireLogic
 */
public class AlphafireLogic {
    
    private static Set<Item> items_without_fortune;

    private static final Supplier<TagKey<Item>> BLACKLIST = TagHelper.memoizedSupplier(() -> TagHelper.item(new Identifier(ModInit.MODID,"alpha_fire_blacklist")));

    private static final OreDrops formula = new OreDrops();

    public static void events() {
    }

    private static boolean hasBonusLootFunction(LootTable lt) {
        for (LootPool pool : lt.pools) {
            // Oh God
            for (LootPoolEntry entry : pool.entries) {
                if (entry.getType() == LootPoolEntryTypes.ALTERNATIVES) { 
                    // Please
                    for (LootPoolEntry lpe : ((AlternativeEntry) entry).children) {
                        for (LootFunction function : ((LeafEntry) lpe).functions) { 
                            // Make it stop
                            if (function instanceof ApplyBonusLootFunction abtf && abtf.enchantment == Enchantments.FORTUNE) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public static void storeExp(ItemStack stack, float value) {
        float stored = stack.getOrCreateNbt().getFloat("alphafireStoredExp");
        stored+=value;
        stack.getOrCreateNbt().putFloat("alphafireStoredExp", stored);
    }
    public static int removeExp(ItemStack stack) {
        float stored = stack.getOrCreateNbt().getFloat("alphafireStoredExp");
        int toReturn = (int) Math.floor(stored);
        stack.getOrCreateNbt().putFloat("alphafireStoredExp", stored-toReturn);
        return toReturn;
    }

    public static void dropExperience(float experienceValue, BlockPos blockPos, World world) {
        world.spawnEntity(new ExperienceOrbEntity(world, blockPos.getX(), blockPos.getY(), blockPos.getZ(), (int)experienceValue));
    }

    private static void initManualLootFunctionItems(ServerWorld world) {
        Set<Block> blocks_without_loot_function = Sets.newHashSet();

        if (items_without_fortune == null) items_without_fortune = Sets.newHashSet();
        items_without_fortune.clear();

        // blocks_without_loot_function.addAll(VeinMinerLogic.ORES.get().values());?
        Named<Block> named = Registries.BLOCK.getEntryList(VeinMinerLogic.ORES.get()).get();
        named.forEach(entry -> blocks_without_loot_function.add(entry.value()));

        named.forEach(entry -> {
            if (hasBonusLootFunction(world.getServer().getLootManager().getTable(entry.value().getLootTableId()))) {
                blocks_without_loot_function.remove(entry.value());
            }
        });
        blocks_without_loot_function.stream().map(b -> b.asItem()).forEach(items_without_fortune::add);
    }

    public static void attemptSmeltStacks(ServerWorld world, Entity entity, ItemStack tool, List<ItemStack> dropList) {
        if (items_without_fortune == null && MoEnchantsConfig.config.ApplyLootingToAlphaFire) {
            initManualLootFunctionItems(world);
            for(Item x : items_without_fortune) {
                ModInit.logger.debug("Added manual fortune item: %s", x.getTranslationKey());
            }
        }
        if (items_without_fortune.isEmpty()) {
            ModInit.logger.error("MoEnchantment#alphafire: Failed to parse blocks_without_loot_function/Failed to create items_without_fortune");
        }

        RecipeManager rm = world.getRecipeManager(); 
        // Inventory basicInv = new SimpleInventory(1);
        Inventory basicInv = new SingletonInventory();

        ItemStack itemToBeChecked = ItemStack.EMPTY;
        Optional<SmeltingRecipe> smeltingResult;

        for (int stacksIndex = 0; stacksIndex < dropList.size(); stacksIndex++) {
            itemToBeChecked = dropList.get(stacksIndex);
            basicInv.setStack(0, itemToBeChecked);
            smeltingResult = rm.getFirstMatch(RecipeType.SMELTING, basicInv, entity.world);
            
            if (smeltingResult.isPresent() && !itemToBeChecked.isIn(BLACKLIST.get())) {
                int count = itemToBeChecked.getCount();
                if (items_without_fortune.contains(itemToBeChecked.getItem()) && EnchantmentHelper.getLevel(Enchantments.FORTUNE, tool) > 0) {
                    count = formula.getValue(world.getRandom(), count, EnchantmentHelper.getLevel(Enchantments.FORTUNE, tool));
                    if (MoEnchantsConfig.config.AlphaFireCauseExtraDamage &&  entity instanceof PlayerEntity pe) {
                        tool.damage(Math.max(count-1, 3), pe,(playerEntity_1)  ->  {
                            playerEntity_1.sendToolBreakStatus(((PlayerEntity)entity).getActiveHand());
                        });
                    }
                }
                dropList.set(stacksIndex, new ItemStack(smeltingResult.get().getOutput().getItem(), count));
                AlphafireLogic.storeExp(tool, smeltingResult.get().getExperience() * itemToBeChecked.getCount());

                if (!ModInit.lockAutoSmeltSound) {
                    ModInit.lockAutoSmeltSound = true;
                    world.playSound((PlayerEntity)null, entity.getBlockPos(), SoundEvents.BLOCK_FIRE_EXTINGUISH, SoundCategory.BLOCKS, 0.1f, 0.1f);
                }
            }
        }
    }
}