package com.biom4st3r.moenchantments.util;

import java.util.function.Supplier;
import java.util.stream.Stream;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.registry.Registry;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.util.Identifier;

public class TagHelper {
    public static <T> Supplier<TagKey<T>> memoizedSupplier(Supplier<TagKey<T>> supplier) {
        return new Supplier<TagKey<T>>() {
            TagKey<T> t;
            Supplier<TagKey<T>> s = supplier;

            @Override
            public TagKey<T> get() {
                if (t == null) {
                    synchronized(this) {
                        if (t == null) {
                            t = s.get();
                        }
                    }
                }
                return t;
            }
        };
    }
    
    public static TagKey<Item> item(String s) {
        return TagKey.of(RegistryKeys.ITEM, new Identifier(s));
    }
    public static TagKey<Block> block(String s) {
        return TagKey.of(RegistryKeys.BLOCK, new Identifier(s));
    }    
    public static TagKey<Item> item(Identifier s) {
        return TagKey.of(RegistryKeys.ITEM, s);
    }
    public static TagKey<Block> block(Identifier s) {
        return TagKey.of(RegistryKeys.BLOCK, s);
    }
    public static <T> Stream<T> toStream(Registry<T> registry, TagKey<T> tag) {
        return registry
            .getEntryList(tag)
            .stream()
            .map(n -> n.stream())
            .flatMap(s -> s)
            .map(entry -> entry.value());
    }

}
