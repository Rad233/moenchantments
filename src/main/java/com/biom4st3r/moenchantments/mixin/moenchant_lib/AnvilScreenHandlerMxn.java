package com.biom4st3r.moenchantments.mixin.moenchant_lib;

import java.util.Map;
import java.util.Map.Entry;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import biom4st3r.libs.moenchant_lib.ExtendedEnchantment;
import biom4st3r.libs.moenchant_lib.LibInit;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.EnchantedBookItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.screen.AnvilScreenHandler;
import net.minecraft.screen.ForgingScreenHandler;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.screen.ScreenHandlerType;

@Mixin({AnvilScreenHandler.class})
public abstract class AnvilScreenHandlerMxn extends ForgingScreenHandler {

    public AnvilScreenHandlerMxn(ScreenHandlerType<?> type, int syncId, PlayerInventory playerInventory,
            ScreenHandlerContext context) {
        super(type, syncId, playerInventory, context);
    }

    @Inject(
        method = "updateResult",
        at = @At(
            value = "INVOKE",
            shift = Shift.BEFORE,
            target = "net/minecraft/screen/AnvilScreenHandler.sendContentUpdates()V"
        )
    )
    private void moenchantlib$controlOutputEnchantments(CallbackInfo ci) {
        if (!LibInit.enableBetterAnvil) return;
        final ItemStack left = this.input.getStack(0);
        final ItemStack right = this.input.getStack(1);
        final ItemStack output = this.output.getStack(0);
        EnchantmentLevelEntry[] enchantEntry = {null};
        boolean hasChanges = false;
        // Lazy loading, because it's kind of expensive
        final Supplier<Map<Enchantment, Integer>> enchantments = Suppliers.memoize(() -> EnchantmentHelper.get(output));

        final boolean isBook = output.getItem() == Items.ENCHANTED_BOOK;

        for (Entry<Enchantment, Integer> e : EnchantmentHelper.get(output).entrySet()) {
            if (!ExtendedEnchantment.cast(e.getKey()).isExtended()) continue;
            ExtendedEnchantment enchantment = (ExtendedEnchantment) e.getKey();

            switch(enchantment.isAcceptibleInAnvil(left, right, output, entry->enchantEntry[0] = entry)) {
                case AMEND:
                    hasChanges = true;
                    enchantments.get().put(enchantEntry[0].enchantment, enchantEntry[0].level);
                    enchantEntry[0] = null;
                    continue;
                case PASS:
                    continue;
                case REJECT:
                    this.output.setStack(0, ItemStack.EMPTY);
                    return;
                case REMOVE:
                    enchantments.get().remove(e.getKey());
                    hasChanges = true;
                    continue;
            }
        }
        if (hasChanges) {
            output.removeSubNbt("Enchantments");
            output.removeSubNbt("StoredEnchantments");
            for(Entry<Enchantment, Integer> entry : enchantments.get().entrySet()) {
                if(isBook) EnchantedBookItem.addEnchantment(output, new EnchantmentLevelEntry(entry.getKey(), entry.getValue()));
                else output.addEnchantment(entry.getKey(), entry.getValue());
            }
        }
    }
    
    @Redirect(
        method = "updateResult",
        at = @At(
            value = "INVOKE", 
            target = "net/minecraft/enchantment/Enchantment.isAcceptableItem(Lnet/minecraft/item/ItemStack;)Z",
            ordinal = 0
        )
    )
    private boolean replaceIsAcceptibleItem(Enchantment enchant, ItemStack is) {
        if (ExtendedEnchantment.cast(enchant).isExtended()) {
            ExtendedEnchantment ex = ExtendedEnchantment.cast(enchant);
            return ex.isAcceptibleInAnvil(is, (AnvilScreenHandler)(Object)this);
        } else {
            return enchant.isAcceptableItem(is);
        }
    }

}
