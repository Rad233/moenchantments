package com.biom4st3r.moenchantments.mixin.moenchant_lib.events;

import java.util.Map.Entry;
import java.util.concurrent.locks.ReentrantLock;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.TypedActionResult;

import biom4st3r.libs.moenchant_lib.ExtendedEnchantment;
import biom4st3r.libs.moenchant_lib.interfaces.EnchantableProjectileEntity;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin(LivingEntity.class)
public abstract class LivingEntityDamageMxn {
    
    private Float moenchantments$builtInNewDamage = null;
    private ReentrantLock moenchantments$builtInLock = new ReentrantLock();

    @Inject(
        at = @At(value = "FIELD", opcode = Opcodes.PUTFIELD, target = "net/minecraft/entity/LivingEntity.despawnCounter:I"),
        method = "damage",
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE
    )
    public void extendedEnchantment$modifyDamage(DamageSource source, float damage, CallbackInfoReturnable<Boolean> ci) {
        final float originalDamage = damage;
        // Damage output
        if (!source.isProjectile() && source.getAttacker() instanceof LivingEntity le) {
            Float output = moenchantment$modifyDamage(le, damage, source, true);
            if (output == null) {
                ci.cancel();
            } else {
                damage = output.floatValue();
            }
        } else if (source.isProjectile() && source.getSource() instanceof PersistentProjectileEntity projectile) {
            Object2IntMap<ExtendedEnchantment> map = EnchantableProjectileEntity.getEnchantments(projectile);
            if (map != null) {
                for (Object2IntMap.Entry<ExtendedEnchantment> entry : map.object2IntEntrySet()) {
                    TypedActionResult<Float> result = entry.getKey().modifyDamageOutput(entry.getIntValue(), (LivingEntity)(Object)this, damage, source);
                    if (result.getResult() == ActionResult.CONSUME) {
                        damage = result.getValue().floatValue();
                    } else if (result.getResult() == ActionResult.FAIL) {
                        ci.cancel();
                    }
                }
            }
        }
        // Incoming Damage
        final LivingEntity THIS = (LivingEntity)(Object)this;
        Float output = moenchantment$modifyDamage(THIS, damage, source, false);
        if (output == null) {
            ci.cancel();
        } else {
            damage = output.floatValue();
        }
        
        if (originalDamage != damage) {
            moenchantments$builtInLock.lock();
            moenchantments$builtInNewDamage = damage;
        }
    }

    private static Float moenchantment$modifyDamage(LivingEntity entity, float damage, DamageSource source, boolean modifyDamageOutput) {
        for (EquipmentSlot slot : EquipmentSlot.values()) {
            ItemStack stack = entity.getEquippedStack(slot);
            for (Entry<Enchantment, Integer> entry : EnchantmentHelper.get(stack).entrySet()) {
                boolean itemStackInProperSlot = entry.getKey().getEquipment(entity).values().contains(stack);
                if (entry.getKey() instanceof ExtendedEnchantment ee && itemStackInProperSlot) {
                    TypedActionResult<Float> result = modifyDamageOutput ? ee.modifyDamageOutput(entry.getValue(), entity, damage, source) : ee.modifyIncomingDamage(entry.getValue(), entity, damage, source);
                    if (result.getResult() == ActionResult.CONSUME) {
                        return result.getValue();
                    } else if (result.getResult() == ActionResult.FAIL) {
                        return null;
                    }
                }
            }
        }
        return damage;
    }

    @ModifyVariable(
        at = @At(value = "FIELD", opcode = Opcodes.PUTFIELD, target = "net/minecraft/entity/LivingEntity.despawnCounter:I", shift = Shift.BY, by = 1),
        method = "damage",
        argsOnly = true,
        index = 2,
        print = false
    )
    public float extendedEnchantment$modifyDamage2(float damage) {
        if (moenchantments$builtInNewDamage != null) {
            damage = moenchantments$builtInNewDamage.floatValue();
            moenchantments$builtInNewDamage = null;
            moenchantments$builtInLock.unlock();
        }
        return damage;
    }
}