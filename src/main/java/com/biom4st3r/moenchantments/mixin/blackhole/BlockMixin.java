package com.biom4st3r.moenchantments.mixin.blackhole;

import java.util.List;

import org.jetbrains.annotations.Nullable;
import org.joml.Vector3f;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.registration.EnchantmentRegistry;

import biom4st3r.libs.particle_emitter.ParticleEmitter;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.particle.DustParticleEffect;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

@Mixin({Block.class})
public class BlockMixin {
    @Inject(
        method = "dropStacks(Lnet/minecraft/block/BlockState;Lnet/minecraft/world/World;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/block/entity/BlockEntity;Lnet/minecraft/entity/Entity;Lnet/minecraft/item/ItemStack;)V", 
        at = @At("HEAD"),
        cancellable = true)
    private static void moenchantments_blackHole_dropStacks(BlockState state, World world, BlockPos pos, @Nullable BlockEntity blockEntity, Entity entity, ItemStack stack, CallbackInfo ci) {
        ModInit.test();
        if (EnchantmentRegistry.BLACK_HOLE.hasEnchantment(stack)) {
            if (world instanceof ServerWorld && entity instanceof PlayerEntity player) {
                List<ItemStack> drops = Block.getDroppedStacks(state, (ServerWorld) world, pos, blockEntity, entity, stack);
                if (drops.size() != 0) {
                    ParticleEmitter.of(new DustParticleEffect(new Vector3f(0, 0, 0), 1), new Vec3d(pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5), "0", "0", "0", world, 1).ifPresent(ParticleEmitter::begin);
                }
                for (ItemStack itemStack : drops) {
                    if (!player.getInventory().insertStack(itemStack)) {
                        Block.dropStack(world, pos, itemStack);
                    }
                }
                ci.cancel();
            }
        }
    }
}
