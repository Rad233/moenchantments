package com.biom4st3r.moenchantments.mixin.compat.adventure_plateform;

import net.minecraft.network.ClientConnection;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.biom4st3r.moenchantments.entities.DummyServerNetworkHandler;

@Mixin({ServerPlayNetworkHandler.class})
public class ServerPlayNetworkHandlerMxn {
    // https://github.com/KyoriPowered/adventure-platform-fabric/blob/d5cd0b5c728051fcdf209536ed800444df366a7d/src/mixin/java/net/kyori/adventure/platform/fabric/impl/mixin/ServerGamePacketListenerImplMixin.java#L44
    // @Inject(at = @At(value = "INVOKE",shift = Shift.AFTER, target = "java/lang/Object.<init>()V"),method = "<init>", cancellable = true)
    // private void preventgetChannel(MinecraftServer minecraftServer, ClientConnection connection, ServerPlayerEntity player, CallbackInfo ci) {
    //     // if ((Object)this instanceof DummyServerNetworkHandler) {
    //     //     ci.cancel();
    //     //     return;
    //     // }
    // }
    // @Inject(at = @At("HEAD"), method = "adventure$initTracking", cancellable = true)
    // private void adventure$initTracking(final MinecraftServer server, final ClientConnection conn, final ServerPlayerEntity player, final CallbackInfo cc, final CallbackInfo ci) {
    //     if ((Object)this instanceof DummyServerNetworkHandler) {
    //         ci.cancel();
    //         return;
    //     }
    // }

}
