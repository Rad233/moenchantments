package com.biom4st3r.moenchantments.mixin;

import net.minecraft.server.dedicated.MinecraftDedicatedServer;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin({MinecraftDedicatedServer.class})
public class SlaughterWatchDogMxn {
    @Redirect(
        method = "setupServer",
        at = @At(
            value = "INVOKE", 
            target = "java/lang/Thread.start()V",
            ordinal = 1))
    private void slaughterWatchDog(Thread thread) {
    }
}
