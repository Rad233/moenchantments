package com.biom4st3r.moenchantments.mixin.newfeatures;

import com.biom4st3r.moenchantments.NewFeatures;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.ArmorMaterials;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ToolItem;
import net.minecraft.item.ToolMaterials;
import net.minecraft.screen.GrindstoneScreenHandler;

@Mixin(targets = {"net/minecraft/screen/GrindstoneScreenHandler$4"})
public class GrindStoneScreenHandlerMxn {

    @Shadow
    @Final
    GrindstoneScreenHandler field_16780;

    @SuppressWarnings({"resource"})
    @Inject(at = @At("HEAD"), method = "onTakeItem")
    public void moenchantments$onTakeItem(PlayerEntity player, ItemStack stack, CallbackInfo ci) {
        if (player.getWorld().isClient) return;
        if (!NewFeatures.new_features) return;
        ItemStack result = stack;
        if (field_16780.getSlot(0).getStack().isDamageable()) stack = field_16780.getSlot(0).getStack();
        else if (field_16780.getSlot(1).getStack().isDamageable()) stack = field_16780.getSlot(1).getStack();

        if (stack.isEmpty()) return;
        // ModInit.test();
        Item item = null;
        if (stack.getItem() instanceof ToolItem tool) {
            if (tool.getMaterial() == ToolMaterials.IRON) {
                item = NewFeatures.INFUSED_IRON_SHAVINGS;
            } else if (tool.getMaterial() == ToolMaterials.GOLD) {
                item = NewFeatures.INFUSED_GOLD_SHAVINGS;
            } else if (tool.getMaterial() == ToolMaterials.NETHERITE) {
                item = NewFeatures.INFUSED_NETHERITE_SHAVINGS;
            }
        } else if (stack.getItem() instanceof ArmorItem armor) {
            if (armor.getMaterial() == ArmorMaterials.IRON || armor.getMaterial() == ArmorMaterials.CHAIN) {
                item = NewFeatures.INFUSED_IRON_SHAVINGS;
            } else if (armor.getMaterial() == ArmorMaterials.GOLD) {
                item = NewFeatures.INFUSED_GOLD_SHAVINGS;
            } else if (armor.getMaterial() == ArmorMaterials.NETHERITE) {
                item = NewFeatures.INFUSED_NETHERITE_SHAVINGS;
            }
        }
        if (item == null) return;
        result.setDamage(stack.getDamage() + (int)(stack.getMaxDamage() * 0.1F));
        if (result.getMaxDamage() - result.getDamage() <= 0) result.setCount(0);
        ItemStack powder = new ItemStack(item, player.getRandom().nextInt(3) + 1);
        player.dropStack(powder);
    }
}
