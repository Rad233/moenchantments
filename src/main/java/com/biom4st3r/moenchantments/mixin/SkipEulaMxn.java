package com.biom4st3r.moenchantments.mixin;

import net.minecraft.server.dedicated.EulaReader;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

/**
 * DevSkipEulaMixin
 */
@Mixin(EulaReader.class)
public abstract class SkipEulaMxn {
    @Inject(
        at = @At("HEAD"), 
        method = "isEulaAgreedTo", 
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    private void eulaIsAgreedTo(CallbackInfoReturnable<Boolean> ci) {
        ci.setReturnValue(true);
    }
}