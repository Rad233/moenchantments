package com.biom4st3r.moenchantments.mixin.endstep;

import com.biom4st3r.moenchantments.interfaces.EndStepper;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;

@Mixin({PlayerEntity.class})
public abstract class PlayerEntityMxn extends LivingEntity {
    protected PlayerEntityMxn(EntityType<? extends LivingEntity> entityType, World world) {
        super(entityType, world);
    }

    @Inject(
        method = "tick",
        at = @At(
            value = "INVOKE",
            target = "net/minecraft/entity/player/PlayerEntity.isSpectator()Z",
            shift = Shift.BEFORE,
            ordinal = 1)
        )
    private void reassign_noClip(CallbackInfo ci) {
        this.noClip |= ((EndStepper)this).moenchantment$getStepProgress().isPresent();
    }
}
