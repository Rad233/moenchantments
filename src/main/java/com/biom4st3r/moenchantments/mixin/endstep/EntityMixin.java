package com.biom4st3r.moenchantments.mixin.endstep;

import java.util.Optional;

import net.minecraft.entity.Entity;
import net.minecraft.entity.MovementType;
import net.minecraft.util.math.Vec3d;

import com.biom4st3r.moenchantments.interfaces.EndStepper;
import com.biom4st3r.moenchantments.logic.EndStepProgressCounter;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin({Entity.class})
public class EntityMixin {
    @Inject(
        at = @At("HEAD"), 
        method = "move", 
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    private void attemptStopEndStepMovement(MovementType type, Vec3d movement, CallbackInfo ci) {
        if (this instanceof EndStepper THIS) {
            Optional<EndStepProgressCounter> o = THIS.moenchantment$getStepProgress();
            if (!o.isPresent()) return;
            ci.cancel();
        }
    }
    @Inject(
        at = @At("HEAD"), 
        method = "requestTeleport", 
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    private void moenchantment$preventTeleportDuringEndStep(double destX, double destY, double destZ, CallbackInfo ci) {
        if (this instanceof EndStepper THIS) {
            Optional<EndStepProgressCounter> i = THIS.moenchantment$getStepProgress();
            if (i.isPresent() && !i.get().canTeleport()) {
                ci.cancel();
            }
        }
    }
}
