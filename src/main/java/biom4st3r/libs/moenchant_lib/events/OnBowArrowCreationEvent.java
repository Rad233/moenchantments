package biom4st3r.libs.moenchant_lib.events;

import org.jetbrains.annotations.ApiStatus.AvailableSince;
import org.jetbrains.annotations.ApiStatus.ScheduledForRemoval;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.item.ItemStack;

@AvailableSince("0.1.0")
public interface OnBowArrowCreationEvent {
    @Deprecated(forRemoval = true)
    @ScheduledForRemoval(inVersion = "0.4.5")
    Event<OnBowArrowCreationEvent> EVENT = EventFactory.createArrayBacked(OnBowArrowCreationEvent.class, listeners ->
    (bow, arrowStack, arrowEntity, user, elapsedUseTime, pullProgress)  ->  {
        for (OnBowArrowCreationEvent invoker : listeners) {
            invoker.onCreation(bow, arrowStack, arrowEntity, user, elapsedUseTime, pullProgress);
        }
    });

    void onCreation(ItemStack bow, ItemStack arrowStack, PersistentProjectileEntity arrowEntity, PlayerEntity user, int elapsedUseTime, float pullProgress);
}