package biom4st3r.libs.moenchant_lib.config_test;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.function.Function;

import biom4st3r.net.objecthunter.exp4j.Expression;
import biom4st3r.net.objecthunter.exp4j.ExpressionBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class EnchantmentOverride implements JsonDeserializer<EnchantmentOverride> {

    public EnchantmentOverride() {
        this(null, null, false, false, false, 1, 1, null, null);
    }

    private EnchantmentOverride(Expression min_power, Expression max_power, Boolean enabled, Boolean treasure,
        Boolean curse, Integer max_level, Integer min_level, String[] exclusives, String rarity) {
        this.min_power = min_power;
        this.max_power = max_power;
        this.enabled = enabled;
        this.treasure = treasure;
        this.curse = curse;
        this.max_level = max_level;
        this.min_level = min_level;
        this.exclusives = exclusives;
        this.rarity = rarity;
        // Rarity ff; // Fuck you java
        // try { // Fuck you java
        //     ff = Enum.valueOf(Rarity.class, rarity); // Fuck you java
        // } catch(Throwable t) { // Fuck you java
        //     ff = null; // Fuck you java
        // } // Fuck you java
        // this.rarity = ff; // Fuck you java
    }

    public final Expression min_power;
    public final Expression max_power;
    public final Boolean enabled;
    public final Boolean treasure;
    public final Boolean curse;
    public final Integer max_level;
    public final Integer min_level;
    public final String[] exclusives;
    public final String rarity;


    public static class JsonHandler {
        private final JsonObject original;
        private JsonObject obj;
        private JsonElement currentElement;
        
        public boolean isInvalid() {
            return original == null;
        }

        public JsonElement getCurrentElement() {
            return currentElement;
        }

        private JsonHandler(JsonObject obj) {
            this.obj = obj;
            this.original = obj;
        }

        public Boolean getAsBoolean(Boolean defaultB) {
            if (currentElement == null) return defaultB;
            return currentElement.getAsBoolean();
        }
        public Integer getAsInt(Integer defaultI) {
            if (currentElement == null) return defaultI;
            return currentElement.getAsInt();
        }
        public String getAsString(String string) {
            if (currentElement == null) return string;
            return currentElement.getAsString();
        }

        public <T> T[] getAsArray(T[] array, T[] defaultVal, Function<JsonElement, T> func) {
            
            if (currentElement == null) return defaultVal;
            JsonArray ja = currentElement.getAsJsonArray();
            array = Arrays.copyOf(array, ja.size());
            for (int i = 0; i < array.length; i++) {
                array[i] = func.apply(ja.get(i));
            }
            return array;
        }

        public JsonHandler get(String s) {
            currentElement = obj.get(s);
            return this;
        }

        public boolean isPresent() {
            return currentElement != null;
        }

        /**
         * Reassigns the currentElemnt to the primary obj
         * @return {@code this}
         */
        public JsonHandler transverse() {
            this.obj = currentElement.getAsJsonObject();
            return this;
        }
        /**
         * shorthand transverse
         * @return {@code this}
         */
        public JsonHandler t() {
            return this.transverse();
        }
        /**
         * resets the primary obj to it's original value
         * @return {@code this}
         */
        public JsonHandler reset() {
            this.currentElement = null;
            this.obj = original;
            return this;
        }

        public static JsonHandler of(JsonObject opj) {
            return new JsonHandler(opj);
        }
    }

    @Override
    public EnchantmentOverride deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {

        JsonHandler obj = JsonHandler.of(json.getAsJsonObject());

        String min = obj.get("min_power").getAsString(null);
        Expression min_power = null;
        String max = obj.get("max_power").getAsString(null); 
        Expression max_power = null;
        if (min != null) min_power = new ExpressionBuilder(min).variable("level").build();
        if (max != null) max_power = new ExpressionBuilder(max).variable("level").build();

        Boolean enabled = obj.get("enabled").getAsBoolean(null);
        Boolean treasure = obj.get("treasure").getAsBoolean(null);
        Boolean curse = obj.get("curse").getAsBoolean(null);
        Integer min_level = obj.get("min_level").getAsInt(null);
        Integer max_level = obj.get("max_level").getAsInt(null);
        String[] exclusives = obj.get("exclusive").getAsArray(new String[0], new String[0], ele  ->  ele.getAsString());
        String rarity = obj.get("rarity").getAsString("").toUpperCase();
        EnchantmentOverride override = new EnchantmentOverride(min_power, max_power, enabled, treasure, curse, max_level, min_level, exclusives, rarity);
        return override;
    }
}
