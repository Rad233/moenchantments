package biom4st3r.libs.moenchant_lib.mixin;

import java.util.Set;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import biom4st3r.libs.moenchant_lib.ExtendedEnchantment;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.item.ItemStack;

@Mixin({ItemStack.class})
public abstract class ItemStackMxn {
    @Shadow
    public abstract boolean hasEnchantments();

    @Inject(
        at = @At("TAIL"), 
        method = "getAttributeModifiers", 
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    private void moenchantlib$modifyAttributeModifiers(EquipmentSlot slot, CallbackInfoReturnable<Multimap<EntityAttribute, EntityAttributeModifier>> ci) {
        // if(contextEntity instanceof ServerPlayerEntity) {
        //     System.out.println(contextEntity);
        // }
        Multimap<EntityAttribute, EntityAttributeModifier> map = ci.getReturnValue();
        if(!this.hasEnchantments())  return; // Early return: if no enchantments
        Set<ExtendedEnchantment> enchants = ExtendedEnchantment.getExtendedEnchantments((ItemStack)(Object)this).keySet();
        if(enchants.isEmpty()) return; // Early return: if no extendedEnchantments
        
        if(map.isEmpty()) map = HashMultimap.create(); // No reason to create and from an empty map
        else if(map instanceof ImmutableMultimap) map = HashMultimap.create(map); // Must be modifiable

        for(ExtendedEnchantment enchant : enchants) {
            map.putAll(enchant.getEntityAttributes(slot));
        }

        if(map.isEmpty()) return; // no reason to cancel if empty

        ci.setReturnValue(map);
    }
}
