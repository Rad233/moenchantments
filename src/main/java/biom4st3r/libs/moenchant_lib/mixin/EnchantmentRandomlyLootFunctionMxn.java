package biom4st3r.libs.moenchant_lib.mixin;

import java.util.List;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import biom4st3r.libs.moenchant_lib.ExtendedEnchantment;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.function.EnchantRandomlyLootFunction;
import net.minecraft.util.math.random.Random;

@Mixin(EnchantRandomlyLootFunction.class)
public abstract class EnchantmentRandomlyLootFunctionMxn {
    @Shadow @Final private List<Enchantment> enchantments;

    @Shadow public abstract ItemStack process(ItemStack stack, LootContext context);

    @Inject(
        at = @At(
            value = "INVOKE",
            target = "net/minecraft/loot/function/EnchantRandomlyLootFunction.addEnchantmentToStack(Lnet/minecraft/item/ItemStack;Lnet/minecraft/enchantment/Enchantment;Lnet/minecraft/util/math/random/Random;)Lnet/minecraft/item/ItemStack;",
            shift = Shift.BEFORE),
        method = "process",
        locals = LocalCapture.CAPTURE_FAILHARD,
        cancellable = true)
    private void biom4st3r_enchantments_control_application(ItemStack stack, LootContext context, CallbackInfoReturnable<ItemStack> ci, Enchantment enchantment2, Random random) {
        // Should I check if any other randomly selectable enchantments exist in the Registry?
        if (enchantments.isEmpty()) {
            if (ExtendedEnchantment.cast(enchantment2).isExtended()) {
                ExtendedEnchantment enchant = ExtendedEnchantment.cast(enchantment2);
                if (!enchant.isAcceptableForRandomLootFunc(stack, context)) {
                    ci.setReturnValue(process(stack, context));
                }
            }
        }
    }
}
