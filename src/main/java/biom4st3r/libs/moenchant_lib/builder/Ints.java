package biom4st3r.libs.moenchant_lib.builder;

enum Ints {
    MIN_LEVEL,
    MAX_LEVEL,
    ;
    public int get(EnchantmentSkeleton skele) {
        return skele.get(this);
    }
    public void set(EnchantmentSkeleton skele, int b) {
        skele.set(this, b);
    }
}