package biom4st3r.libs.moenchant_lib.interfaces;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.entity.projectile.PersistentProjectileEntity;

import biom4st3r.libs.moenchant_lib.ExtendedEnchantment;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import org.jetbrains.annotations.ApiStatus.AvailableSince;

@AvailableSince("0.1.0")
public interface EnchantableProjectileEntity  {
    @AvailableSince("0.1.0")
    public void setEnchantmentLevel(Enchantment e, int level);

    @AvailableSince("0.2.0")
    public void setEnchantmentLevel(ExtendedEnchantment e, int level);

    @AvailableSince("0.1.0")
    public int getEnchantmentLevel(Enchantment e);

    @AvailableSince("0.2.0")
    public int getEnchantmentLevel(ExtendedEnchantment e);

    @AvailableSince("0.1.2")
    static void enchantProjectile(PersistentProjectileEntity entity, Enchantment enchant, int level) {
        ((EnchantableProjectileEntity)entity).setEnchantmentLevel(enchant, level);
    }

    @AvailableSince("0.1.2")
    static void enchantProjectile(PersistentProjectileEntity entity, EnchantmentLevelEntry entry) {
        enchantProjectile(entity, entry.enchantment, entry.level);
    }

    @AvailableSince("0.1.2")
    static Object2IntMap<ExtendedEnchantment> getEnchantments(PersistentProjectileEntity entity) {
        Object2IntMap<ExtendedEnchantment> enchantments = null;
        for (ExtendedEnchantment e : ExtendedEnchantment.BOW_ENCHANTMENTS) {
            int i = e.getLevelFromProjectile(entity);
            if (i > 0) {
                if (enchantments == null) enchantments = new Object2IntOpenHashMap<>();
                enchantments.put(e, i);
            }
        }
        return enchantments;
    }
}