package biom4st3r.libs.moenchant_lib;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Stream;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

import org.jetbrains.annotations.ApiStatus.AvailableSince;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.ApiStatus.ScheduledForRemoval;

import biom4st3r.libs.moenchant_lib.events.LivingEntityDamageEvent;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentLevelEntry;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContext;
import net.minecraft.screen.AnvilScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.BlockPos;

@AvailableSince("0.1.0")
public interface ExtendedEnchantment {

    @Internal
    public static EnchantmentTarget target = AsmEnchantmentTarget.addTarget("MO_ENCHANT_LIB_DUMMY", Stream.of(AsmEnchantmentTarget.class.getDeclaredMethods()).filter(m->m.getName().equals("do0False0")).findFirst().get());

    public static interface TriConsumer<A, B, C> {
        void accept(A a, B b, C c);
    }

    public static interface ModifyDamageFunction {
        TypedActionResult<Float> modifyDamage(int level, LivingEntity target, float damage, DamageSource b);
    }

    @AvailableSince("0.1.1")
    public static final Map<ExtendedEnchantment, TrackedData<Byte>> TRACKED_ARROW_DATA_KEYS = Maps.newHashMap();
    @AvailableSince("0.1.1")
    public static final List<ExtendedEnchantment> BOW_ENCHANTMENTS = Lists.newArrayList();
    
    @AvailableSince("0.1.0")
    static Optional<ExtendedEnchantment> isExtended(Enchantment e) {
        return ExtendedEnchantment.cast(e).isExtended() ? Optional.of((ExtendedEnchantment) e) : Optional.empty();
    }
    
    @AvailableSince("0.2.0")
    @SuppressWarnings({"unchecked","rawtypes"})
    static Map<ExtendedEnchantment,Integer> getExtendedEnchantments(ItemStack itemStackMxn) {
        Map<Enchantment, Integer> map = EnchantmentHelper.get(itemStackMxn);
        map.entrySet().removeIf(entry->!cast(entry.getKey()).isExtended());
        return (Map) map;
    }

    @AvailableSince("0.2.0")
    boolean isEnabled();

    @AvailableSince("0.2.0")
    Multimap<EntityAttribute, EntityAttributeModifier> getEntityAttributes(EquipmentSlot slot);

    /**
     * Wheather or not it uses extended features
     * @return
     */
    @AvailableSince("0.1.0")
    boolean isExtended();

    /**
     * When enchantment {@code ExtendedEnchantment#isExtended()} this is <p>
     * used to replace {@code EnchantmentTarget#isAcceptableItem(net.minecraft.item.Item)}
     * in {@code EnchantmentHelper#getPossibleEntries(int, ItemStack, boolean)}
     * @param is
     * @return
     */
    @AvailableSince("0.1.0")
    boolean isAcceptable(ItemStack is);

    @AvailableSince("0.4.2")
    public static enum AnvilAction {
        /**
         * Does nothing.
         */
        PASS,
        /**
         * supply changes to the enchantment
         */
        AMEND,
        /**
         * Removes this enchantment from output
         */
        REMOVE,
        /**
         * No output is show in anvil
         */
        REJECT,
    }

    @AvailableSince("0.4.2")
    public static interface AnvilAcceptibleContext {
        AnvilAction check(ItemStack left, ItemStack right, ItemStack output, Consumer<EnchantmentLevelEntry> consumer);
    }

    @AvailableSince("0.4.2")
    AnvilAction isAcceptibleInAnvil(ItemStack left, ItemStack right, ItemStack output, Consumer<EnchantmentLevelEntry> consumer);

    @AvailableSince("0.4.2")
    Text moenchant$getName(int lvl);

    /**
     * When enchantment {@code ExtendedEnchantment#isExtended()} this is <p>
     * used to replace {@code Enchantment#isAcceptableItem(ItemStack)} 
     * in {@code AnvilScreenHandler#updateResult()}
     * @param is
     * @param anvil
     * @return
     */
    @AvailableSince("0.1.0")
    @Deprecated(forRemoval = true)
    @ScheduledForRemoval(inVersion = "0.4.5")
    boolean isAcceptibleInAnvil(ItemStack is, AnvilScreenHandler anvil);

    /**
     * When enchantment {@code ExtendedEnchantment#isExtended()} this is <p>
     * checked in {@code EnchantmentHelper#getPossibleEntries(int, ItemStack, boolean)}<p>
     * in addition to of stack.getItem() == Items.BOOK.
     * @return
     */
    @AvailableSince("0.1.0")
    boolean isAcceptibleOnBook();

    /**
     * gets the tracked data used by the enchantment was added to a projectile entity.
     * @return
     */
    @AvailableSince("0.1.1")
    TrackedData<Byte> getProjectileEnchantmentTrackedData();

    @AvailableSince("0.1.1")
    String regName();

    /**
     * Wheather or not this enchantment is valid for EnchantRandomlyLootFunction
     * @param stack
     * @param context
     * @return
     */
    @AvailableSince("0.1.0")
    boolean isAcceptableForRandomLootFunc(ItemStack stack, LootContext context);

    /**
     * Helper Method
     * @return
     */
    @AvailableSince("0.1.0")
    default Enchantment asEnchantment() {
        return ((Enchantment)this);
    }

    /**
     * Helper Method
     * @param e
     * @return
     */
    @AvailableSince("0.1.0")
    static ExtendedEnchantment cast(Enchantment e) {
        return (ExtendedEnchantment)e;
    }

    /**
     * Whether or not this enchantment has it's own custom conditions for being applied via enchantment table
     * @return
     */
    @AvailableSince("0.1.0")
    default boolean providesApplicationLogic() {
        return false;
    }
    
    /**
     * if {@code ExtendedEnchantment#providesApplicationLogic()} <p>
     * this will fully replace logic in {@code getPossibleEnchantments}<p>
     * for your enchantment.
     * @param power
     * @param stack
     * @param treasureAllowed
     * @param entries
     */
    @AvailableSince("0.1.0")
    default void applicationLogic(Enchantment enchantment, int power, ItemStack stack, boolean treasureAllowed, List<EnchantmentLevelEntry> entries) {
        
    }

    /**
     * Called after an enchantment has been added to the tag of the stack AKA at the end of {@link ItemStack#addEnchantment(Enchantment, int)}
     * @param stack
     */
    @AvailableSince("0.1.0")
    default void enchantmentAddedToStack(ItemStack stack) {}

    
    @AvailableSince("0.1.1")
    default TypedActionResult<Float> modifyIncomingDamage(int level, LivingEntity target, float damage, DamageSource source) {
        return LivingEntityDamageEvent.PASS;
    }

    @AvailableSince("0.1.1")
    default TypedActionResult<Float> modifyDamageOutput(int level, LivingEntity target, float damage, DamageSource source) {
        return LivingEntityDamageEvent.PASS;
    }

    @AvailableSince("0.1.1")
    default int getLevelFromProjectile(ProjectileEntity entity) {
        if (this.getProjectileEnchantmentTrackedData() == null) return 0;
        if (entity == null) return 0;
        return entity.getDataTracker().get(this.getProjectileEnchantmentTrackedData());
    }

    @AvailableSince("0.1.1")
    default void setLevelFromProjectile(ProjectileEntity entity, int level) {
        if (this.getProjectileEnchantmentTrackedData() == null) return;
        if (entity == null) return;
        entity.getDataTracker().set(this.getProjectileEnchantmentTrackedData(), (byte)level);
    }

    @AvailableSince("0.2.0")
    default int getLevel(ItemStack is) {
        return EnchantmentHelper.getLevel(this.asEnchantment(), is);
    }

    @AvailableSince("0.2.0")
    default int getLevel(LivingEntity e) {
        return EnchantmentHelper.getEquipmentLevel(this.asEnchantment(), e);
    }

    @AvailableSince("0.2.0")
    default boolean hasEnchantment(ItemStack i) {
        return EnchantmentHelper.getLevel(this.asEnchantment(), i) > 0;
    }

    /**
     * When an arrow is created by a bow this will have the enchantment copied onto the arrow. It can be checked via {@link ExtendedEnchantment#getLevelFromProjectile(ProjectileEntity)}
     * @return
     */
    @AvailableSince("tbd")
    default boolean transfersFromBowToArrow() {
        return false;
    }

    /**
     * When an arrow is created by a bow this will have the enchantment copied onto the arrow. It can be checked via {@link ExtendedEnchantment#getLevelFromProjectile(ProjectileEntity)}
     * @return
     */
    @AvailableSince("tbd")
    default boolean transfersFromCrossbowToArrow() {
        return false;
    }

    /**
     * Called before a block has been broken by an itemstack with this enchantment.
     * @param mainHandStack
     * @param player
     * @param intValue
     * @return false will cancel the break and cancel further enchantment preBlockBreaks
     */
    @AvailableSince("tbd")
    default boolean preBlockBreak(ServerPlayerEntity player, ItemStack tool, int level, BlockPos pos) {
        return true;
    }

    /**
     * Called after a block has been broken
     * @param mainHandStack
     * @param player
     * @param intValue
     * @return false with cancel further enchantment postBlockBreaks
     */
    @AvailableSince("tbd")
	default boolean postBlockBreak(ServerPlayerEntity player, ItemStack tool, int level, BlockPos pos) {
        return true;
    }

    /**
     * Called when an arrow has been created from a Crossbow
     * @param arrow
     * @param crossBow
     * @param arrowItem
     * @param shooter
     */
    @AvailableSince("tbd")
    default void onCrossbowArrowCreation(PersistentProjectileEntity arrow, ItemStack crossBow, ItemStack arrowItem, LivingEntity shooter) {
        
    }

    /**
     * Called when an arrow has been created from a bow
     * @param arrow
     * @param crossBow
     * @param arrowItem
     * @param shooter
     */
    @AvailableSince("tbd")
    default void onBowArrowCreation(ItemStack bow, ItemStack arrowStack, PersistentProjectileEntity arrowEntity, PlayerEntity user, int elapsedUseTime, float pullProgress) {
        
    }
}
