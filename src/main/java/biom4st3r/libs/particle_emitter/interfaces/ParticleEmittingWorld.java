package biom4st3r.libs.particle_emitter.interfaces;

import biom4st3r.libs.particle_emitter.ParticleEmitter;

public interface ParticleEmittingWorld {
    void addEmitter(ParticleEmitter emitter);
    void markAsDead(long id);
}
