package biom4st3r.libs.particle_emitter.mixins;

import java.util.List;

import com.google.common.collect.Lists;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import biom4st3r.libs.particle_emitter.ParticleEmitter;
import biom4st3r.libs.particle_emitter.interfaces.ParticleEmittingWorld;
import net.minecraft.world.World;

@Mixin({World.class})
public class WorldParticleEmitterMnx implements ParticleEmittingWorld {
    public List<ParticleEmitter> particle_emitters$emitters = Lists.newArrayList();

    @Inject(
        at = @At("HEAD"), 
        method = "tickBlockEntities", 
        cancellable = false,
        locals = LocalCapture.NO_CAPTURE)
    private void tickEmitters(CallbackInfo ci) { 
        // Just needs to be ticked. being in blockentities doesn't matter
        for (ParticleEmitter emitter : particle_emitters$emitters.toArray(new ParticleEmitter[0])) { // Avoid concurrent modification errors
            if (emitter.isDead()) {
                particle_emitters$emitters.remove(emitter);
            } else {
                emitter.tick();
            }
        }
    }

    @Override
    public void addEmitter(ParticleEmitter emitter) {
        particle_emitters$emitters.add(emitter);
    }

    @Override
    public void markAsDead(long id) {
        for (ParticleEmitter i : particle_emitters$emitters.toArray(new ParticleEmitter[0])) {
            if (i.ID == id) i.kill();
        }
    }
}
